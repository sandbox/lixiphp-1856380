<?php
/**
 * @file
 * This file provides module theme function.
 */

/**
 * Template preprocess for weibo_connect_login_widget.
 */
function template_preprocess_weibo_connect_login_widget(&$vars) {
  $element = $vars['element'];
  $vars['widget_id'] = $element['#weibo_connect_id'];
  $vars['title'] = variable_get('weibo_connect_login_widget_title', t('Or log in with...'));
  $vars['description'] = '';
  $vars['providers'] = array();
  if ($element['#weibo_connect_destination']) {
    $query = array('destination' => $element['#weibo_connect_destination']);
  }
  else {
    $query = drupal_get_destination();
  }
  $required = !empty($element['#required']) ? '<span class="form-required" title="' . $t('This field is required.') . '">*</span>' : '';
  foreach (weibo_connect_get_enabled_providers() as $provider_id => $provider) {
    $vars['providers'][$provider_id] = l(
      theme('weibo_provider_icon',
        array(
          'module' => $provider['module'],
          'icon_size' => $element['#weibo_connect_login_widget_icon_size'],
          'provider_id' => $provider_id,
          'provider_name' => $provider['title'],
        )
      ),
      'weibo_connect/redirect/' . $provider_id,
      array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'weibo-widget-provider-' . $provider_id,
          'title' => $provider['title'],
          'class' => 'weibo-widget-provider',
        ),
      )
    );
  }
  drupal_add_css(drupal_get_path('module', 'weibo_connect') . '/css/weibo_connect.css');
  if ($element['#weibo_connect_widget_type'] == 'window') {
    drupal_add_js(drupal_get_path('module', 'weibo_connect') . '/js/weibo_connect.js');
  }
}
