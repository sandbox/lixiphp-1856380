<?php
/**
 * @file
 * This file provides page callback function.
 */

/**
 * Weibo provider authorize callback creating account.
 */
function weibo_connect_oauthapi_create($provider_id, $provider_identifier) {
  $providers = weibo_connect_get_enabled_providers();
  if (!isset($provider_id) || !isset($providers[$provider_id])) {
    drupal_not_found();
  }
  // Authenticate this page first.
  if ($weibo_connect = weibo_connect_get_instance($provider_id, $providers[$provider_id]['module'])) {
    $weibo_connect->authenticate();
  }
  else {
    drupal_set_message(t('There was an error processing your request.'), 'error');
    _weibo_connect_close();
  }
  $data = array(
    'provider' => $provider_id,
    'identifier' => $provider_identifier,
  );
  $identity = _weibo_connect_identity_load($data);
  $form = drupal_get_form('weibo_connect_oauthapi_create_form', $identity);
  return $form;
}

/**
 * Weibo provider authorize callback binding account.
 */
function weibo_connect_oauthapi_binding($provider_id, $provider_identifier) {
  $providers = weibo_connect_get_enabled_providers();
  if (!isset($provider_id) || !isset($providers[$provider_id])) {
    drupal_not_found();
  }
  // Authenticate this page first.
  if ($weibo_connect = weibo_connect_get_instance($provider_id, $providers[$provider_id]['module'])) {
    $weibo_connect->authenticate();
  }
  else {
    drupal_set_message(t('There was an error processing your request.'), 'error');
    _weibo_connect_close();
  }
  $data = array(
    'provider' => $provider_id,
    'identifier' => $provider_identifier,
  );
  $identity = _weibo_connect_identity_load($data);
  $form = drupal_get_form('weibo_connect_oauthapi_bind_form', $identity);
  return $form;
}

/**
 * Weibo provider authorize callback creating account form.
 */
function weibo_connect_oauthapi_create_form(&$form_state, $data) {
  $form = array();
  $form['_identity'] = array('#type' => 'value', '#value' => $data);
  $form['fset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required information'),
    '#description' => t('Please fill in additional information to complete your registration.'),
  );
  $form['fset']['tab'] = array(
    '#value' => '<div class="form-item"><label for="tab">' . l(t('Existing %site account? Click here to bind my account.', array('%site' => variable_get('site_name', 'Drupal'))), 'weibo_connect/oauthapi/binding/' . $data['provider'] . '/' . $data['provider_identifier'], array('html' => TRUE)) . "</label></div>",
  );
  $form['fset']['messages'] = array(
    '#value' => '<div class="form-item"><p class="tit-tips"><b class="orange1">' . t('Tips:') . '</b> <span class="fc6">' . t('You are registering the %s website account, after the completion of please verify your email address!', array('%s' => variable_get('site_name', 'Drupal'))) . '</span></p></div>',
  );
  if (variable_get('weibo_connect_registration_username_change', 1)) {
    $form['fset']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $edit['name'],
      '#maxlength' => USERNAME_MAX_LENGTH,
      '#attributes' => array('class' => 'username'),
      '#default_value' => _weibo_connect_make_username($data),
      '#description' => t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
      '#required' => TRUE,
    );
  }
  $form['fset']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#default_value' => $edit['mail'],
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#description' => t('A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new account'),
    '#weight' => 30,
  );
  $form['#validate'][] = 'user_register_validate';
  drupal_add_css(drupal_get_path('module', 'weibo_connect') . '/css/weibo_connect.css');
  return $form;
}

/**
 * See weibo_connect_oauthapi_create_form.
 */
function weibo_connect_oauthapi_create_form_submit($form, &$form_state) {
  global $base_url;
  $identity = $form_state['values']['_identity'];
  $mail = $form_state['values']['mail'];
  $name = $form_state['values']['name'];
  $pass = user_password();
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  if (isset($form_state['values']['roles'])) {
    // Remove unset roles.
    $roles = array_filter($form_state['values']['roles']);
  }
  else {
    $roles = array();
  }
  $merge_data = array('pass' => $pass, 'init' => $mail, 'roles' => $roles);
  $merge_data['status'] = variable_get('user_register', 1) == 1;
  $account = user_save('', array_merge($form_state['values'], $merge_data));
  // Terminate if an error occured during user_save().
  if (!$account) {
    drupal_set_message(t("Error saving user account."), 'error');
    $form_state['redirect'] = '';
    return;
  }
  db_query("UPDATE {weibo_connect_identity} set uid = %d WHERE id = %d", $account->uid, $identity['id']);
  watchdog('user', 'New user from Weibo: %name (%email).', array('%name' => $name, '%email' => $mail), WATCHDOG_NOTICE, l(t('edit'), 'user/' . $account->uid . '/edit'));
  $account->password = $pass;
  if (!variable_get('user_email_verification', TRUE) && $account->status) {
    // No e-mail verification is required, create new user account, and login
    // user immediately.
    _user_mail_notify('register_no_approval_required', $account);
    if (user_authenticate(array_merge($form_state['values'], $merge_data))) {
      drupal_set_message(t('Registration successful. You are now logged in.'));
    }
    $form_state['redirect'] = '';
    return;
  }
  elseif ($account->status) {
    // Create new user account, no administrator approval required.
    $op = $notify ? 'register_admin_created' : 'register_no_approval_required';
    _user_mail_notify($op, $account);
    if ($notify) {
      drupal_set_message(t('Password and further instructions have been e-mailed to the new user <a href="@url">%name</a>.', array('@url' => url("user/$account->uid"), '%name' => $account->name)));
    }
    else {
      drupal_set_message(t('Your password and further instructions have been sent to your e-mail address.'));
      $form_state['redirect'] = '';
      return;
    }
  }
  else {
    // Create new user account, administrator approval required.
    _user_mail_notify('register_pending_approval', $account);
    drupal_set_message(t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your e-mail address.'));
    $form_state['redirect'] = '';
    return;
  }
}

/**
 * Weibo provider authorize callback binding account form.
 */
function weibo_connect_oauthapi_bind_form(&$form_state, $data) {
  global $user;
  // If we are already logged on, go to the user page instead.
  if ($user->uid) {
    drupal_goto('user/' . $user->uid);
  }
  $providers = _weibo_connect_providers_list();
  $img_path = url(drupal_get_path('module', $providers[$data['provider']]['module']) . '/icons/' . variable_get('weibo_connect_login_widget_icon_size', '32_32') . '/' . $data['provider'] . '.png');
  $form = array();
  $form['_identity'] = array('#type' => 'value', '#value' => $data);
  $form['fset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required information'),
    '#description' => t('Please login to complete your binding.'),
  );
  $form['fset']['tab'] = array(
    '#value' => '<div class="form-item"><label for="tab">' . l(t('No account? Click here to register %site account.', array('%site' => variable_get('site_name', 'Drupal'))), 'weibo_connect/oauthapi/create/' . $data['provider'] . '/' . $data['provider_identifier'], array('html' => TRUE)) . "</label></div>",
  );
  $form['fset']['messages'] = array(
    '#value' => '<div class="form-item"><p class="tit-tips"><b class="orange1">' . t('Tips:') . '</b> <span class="fc6">' . t('You are binding the %s website account, after binding two account all can login %s website!', array('%s' => variable_get('site_name', 'Drupal'))) . '</span></p></div>',
  );
  $form['fset']['weibo'] = array(
    '#value' => '<div class="form-item"><p class="weibo_account"><b class="gray1">' . t('You used account') . '</b><br /><br /><span>' . t($providers[$data['provider']]['title']) . '：<img src="' . $img_path . '" alt="' . t($providers[$data['provider']]['title']) . '" align="absmiddle" /></span>' . $data['lastName'] . '</p></div>',
  );
  $form['fset']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#required' => TRUE,
    '#description' => t('Enter your @s username.', array('@s' => variable_get('site_name', 'Drupal'))),
  );
  $form['fset']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Enter the password that accompanies your username.'),
    '#required' => TRUE,
  );
  $form['#validate'] = user_login_default_validators();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Log in'),
    '#weight' => 2,
  );
  drupal_add_css(drupal_get_path('module', 'weibo_connect') . '/css/weibo_connect.css');
  return $form;
}

/**
 * See weibo_connect_oauthapi_bind_form.
 */
function weibo_connect_oauthapi_bind_form_submit($form, &$form_state) {
  global $user;
  $identity = $form_state['values']['_identity'];
  if ($user->uid) {
    db_query("UPDATE {weibo_connect_identity} set uid = %d WHERE id = %d", $user->uid, $identity['id']);
    drupal_set_message(t('Thank you for binding to %name account.', array('%name' => $user->name)));
    $form_state['redirect'] = 'user/' . $user->uid;
    return;
  }
}

/**
 * Weibo callback page.
 */
function weibo_connect_redirect($provider_id) {
  $providers = weibo_connect_get_enabled_providers();
  if (!isset($provider_id) || !isset($providers[$provider_id])) {
    drupal_not_found();
  }
  // Try to get provider object instance.
  if ($weibo_connect = weibo_connect_get_instance($provider_id, $providers[$provider_id]['module'])) {
    _weibo_connect_weibo_auth($weibo_connect, $provider_id);
  }
  else {
    drupal_set_message(t('There was an error processing your request.'), 'error');
    _weibo_connect_close();
  }
}

/**
 * Weibo callback start authorize.
 */
function _weibo_connect_weibo_auth($controller, $provider_id) {
  $params = array();
  try {
    $controller->authenticate($provider_id, $params);
    $profile = (array) ($controller->getUserProfile());
  }
  catch (Exception $e) {
    $redirect = TRUE;
    switch ($e->getCode()) {
      case 5:
        // Authentification failed.
        // The user has canceled the authentication.
        // Or the provider refused the connection.
        $redirect = FALSE;
        break;

      case 0:
        // Unspecified error.
      case 1:
        // Configuration error.
      case 2:
        // Provider not properly configured.
      case 3:
        // Unknown or disabled provider.
      case 4:
        // Missing provider application credentials.
        // Your application id, key or secret.
      case 6:
        // User profile request failed.
      case 7:
        // User not connected to the provider.
      case 8:
        // Provider does not support this feature.
      default:
        // Report the error and log it.
        drupal_set_message(t('@message', array('@message' => $e->getMessage())), 'error');
    }
    _weibo_connect_close();
  }
  // Process Drupal authentication.
  $profile['provider'] = $provider_id;
  _weibo_connect_process_auth($profile);
}

/**
 * Weibo callback process authorize.
 */
function _weibo_connect_process_auth($data) {
  global $user;
  // User is already logged in, tries to add new identity.
  if (user_is_logged_in()) {
    // Identity is already registered.
    if ($identity = _weibo_connect_identity_load($data)) {
      // Registered to this user.
      if ($user->uid == $identity['uid']) {
        drupal_set_message(t('You have already registered this identity.'));
        _weibo_connect_identity_save($data);
        _weibo_connect_close();
      }
      // Registered to another user.
      else {
        drupal_set_message(t('This identity is registered to another user.'), 'error');
        _weibo_connect_close();
      }
    }
    // Identity is not registered - register it to the logged in user.
    else {
      _weibo_connect_identity_save($data);
      drupal_set_message(t('New identity added.'));
      _weibo_connect_close();
    }
  }
  $identity = _weibo_connect_identity_load($data);
  if ($identity && $identity['uid'] > 0) {
    $account = user_load($identity['uid']);
    // Check if user is blocked.
    if (!$account->status) {
      drupal_set_message(t('The username %name has not been activated or is blocked.', array('%name' => $account->name)), 'error');
    }
    // Check for email verification timestamp.
    elseif (!$account->login) {
      drupal_set_message(t('You need to verify your e-mail address - @email.', array('@email' => $account->mail)), 'error');
      drupal_set_message(t('A welcome message with further instructions has been sent to your e-mail address.'));
      _user_mail_notify('register_no_approval_required', $account);
    }
    else {
      $user = $account;
      $form_values = (array) $account;
      unset($form_values['pass']);
      user_authenticate_finalize($form_values);
    }
  }
  elseif ($data['identifier']) {
    _weibo_connect_identity_save($data);
    // Default to go to create account page.
    drupal_goto('weibo_connect/oauthapi/create/' . $data['provider'] . '/' . $data['identifier']);
  }
  _weibo_connect_close();
}

/**
 * Weibo callback close.
 */
function _weibo_connect_close($redirect = TRUE) {
  $weibo_connect_widget_type = variable_get('weibo_connect_widget_type', 'link');
  if ($weibo_connect_widget_type == 'window') {
    _weibo_connect_popup_close($redirect);
  }
  elseif ($weibo_connect_widget_type == 'link') {
    _weibo_connect_link_close($redirect);
  }
}

/**
 * Weibo callback close a page.
 */
function _weibo_connect_link_close($redirect = TRUE) {
  global $user;
  // Keep old path for reference, and to allow forms to redirect to it.
  if (!isset($_REQUEST['destination'])) {
    $_REQUEST['destination'] = 'user';
  }
  $destination = variable_get('weibo_connect_destination', $_REQUEST['destination']);
  drupal_goto(url($destination, array('absolute' => TRUE)));
}

/**
 * Weibo callback close popup window.
 */
function _weibo_connect_popup_close($redirect = TRUE) {
  global $user;
  // Keep old path for reference, and to allow forms to redirect to it.
  if (!isset($_REQUEST['destination'])) {
    $_REQUEST['destination'] = 'user';
  }
  $destination = variable_get('weibo_connect_destination', $_REQUEST['destination']);
  drupal_add_js('
    var redirect = ' . ($redirect ? 'true' : 'false') . ';
    if (window.opener && redirect){
      window.opener.parent.location.href = "' . url($destination, array('absolute' => TRUE)) . '";
    }
    window.self.close();
  ', 'inline');
  $return = 'Closing...';
  print theme('page', $return, FALSE);
}
