<?php
/**
 * @file
 * These are the hooks that are invoked by the Weibo Connect core.
 */

/**
 * Register callbacks for weibo providers list.
 *
 * Allows users to connect with Weibo account.
 * Publish Drupal content as a Weibo.
 * Collect the information from the weibo provider.
 *
 * @return array
 *   An array of weibo providers list.
 */
function hook_weibo_provider() {
  $providers[] = array(
    'id' => 'sina',
    'title' => t('Sina Weibo'),
    'desc' => t('Sina Weibo as default provider.'),
    'callback' => 'weibo_connect_sina_provider',
    'weight' => 1,
    'weibo' => TRUE,
    'module' => 'module_name',
  );
  return $providers;
}
