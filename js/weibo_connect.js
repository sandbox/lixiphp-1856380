Drupal.behaviors.weiboConnectWidget = function (context, settings) {
  $('.weibo-widget-provider', context).click(function(event) {
    event.preventDefault();
    var url = $(this).attr('href');
    popup_window = window.open(url, 'weibo_connect', 'location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no,channelmode=yes,fullscreen=yes,width=800,height=500');
    popup_window.focus();
    return false;
  });
};
