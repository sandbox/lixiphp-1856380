README.txt
==========

Installation
-------------

1. Upload the module to the /sites/all/modules/ folder of your site.
2. Get the sina weibo sdk from 
   http://libweibo.googlecode.com/files/weibo-phpsdk-v2-2011-12-16.zip
	 and extract the saetv2.ex.class.php into the /sites/all/libraries/sina folder.
3. Get the consumer key and secret from weibo open platform.
4. Config your weibo consumer key and secrect 
   at admin/user/settings/weibo_connect

   
Author
-------

LixiPHP <www at lixiphp DOT com>
