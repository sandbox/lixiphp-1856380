<?php
/**
 * @file
 * This file provides QQ weibo provider class.
 */

/**
 * Class WeiboProviderQq.
 * WeiboProvider is fixed, Qq is the weibo provider name.
 */
class WeiboProviderQq {
  public $userId;
  protected $config;
  public static $clientId = '';
  public static $clientSecret = '';
  protected static $accessTokenURL = 'https://open.t.qq.com/cgi-bin/oauth2/access_token';
  protected static $authorizeURL = 'https://open.t.qq.com/cgi-bin/oauth2/authorize';
  public static $apiUrlHttp = 'http://open.t.qq.com/api/';
  public static $apiUrlHttps = 'https://open.t.qq.com/api/';

  /**
   * Construct class.
   */
  public function __construct($config) {
    $this->config = $config;
    if (ini_get('arg_separator.output') == '&amp;') {
      ini_set('arg_separator.output', '&');
    }
    $this->initialize();
  }

  /**
   * Initializer class.
   */
  public function initialize() {
    if (!$this->config['keys']['key'] || !$this->config['keys']['secret']) {
      throw new Exception('Your application key and secret are required in order to connect to ' . $this->config['id'] . '.', 4);
    }
    self::$clientId = $this->config['keys']['key'];
    self::$clientSecret = $this->config['keys']['secret'];
    if ($this->getToken('access_token') && $this->getToken('openid')) {
      $response = $this->api('user/info');
      $user = json_decode($response, TRUE);
      $this->user->profile = (object) $user['data'];
      $this->userId = $user['data']['name'];
    }
  }

  /**
   * Start authenticate.
   */
  public function authenticate() {
    global $base_url;
    $callback = $base_url . '/weibo_connect/redirect/qq';
    if (isset($this->userId) && $this->userId) {
      return;
    }
    elseif ($_GET['code']) {
      // Get access token.
      $code = $_GET['code'];
      $openid = $_GET['openid'];
      $openkey = $_GET['openkey'];
      $url = $this->getAccessToken($code, $callback);
      $r = drupal_http_request($url);
      try {
        parse_str($r->data, $out);
      }
      catch (Exception $e) {
        throw new Exception('User profile request failed ' . $e->messages . '.', 6);
      }
      // Set the session.
      if ($out['access_token']) {
        $_SESSION['t_' . $this->config['id'] . '_access_token'] = $out['access_token'];
        $_SESSION['t_' . $this->config['id'] . '_refresh_token'] = $out['refresh_token'];
        $_SESSION['t_' . $this->config['id'] . '_expire_in'] = $out['expire_in'];
        $_SESSION['t_' . $this->config['id'] . '_code'] = $code;
        $_SESSION['t_' . $this->config['id'] . '_openid'] = $out['openid'];
        $_SESSION['t_' . $this->config['id'] . '_openkey'] = $openkey;
        // Validate.
        $response = $this->api('user/info');
        $user = json_decode($response, TRUE);
        if ($user) {
          $this->user->profile = (object) $user['data'];
          $this->userId = $user['data']['name'];
        }
        else {
          exit('<h3>授权失败,请重试</h3>');
        }
      }
      else {
        exit('<h3>授权失败,请重试</h3>');
      }
    }
    else {
      $url = $this->getAuthorizeURL($callback);
      header('Location: ' . $url);
      exit;
    }
  }

  /**
   * Load the user profile from the weibo api client.
   */
  public function getUserProfile() {
    if (!$this->userId) {
      throw new Exception('User profile request failed. ', 6);
    }
    $profile = array(
      'name' => NULL,
      'first_name' => NULL,
      'nick' => NULL,
      'introduction' => NULL,
      'lang' => NULL,
      'location' => NULL,
      'head' => NULL,
      'homepage' => NULL,
      'age' => NULL,
      'birth_day' => NULL,
      'birth_month' => NULL,
      'birth_year' => NULL,
      'sex' => NULL,
      'email' => NULL,
      'phone' => NULL,
      'address' => NULL,
      'country' => NULL,
      'region' => NULL,
      'city' => NULL,
      'zip' => NULL,
      'fansnum' => 0,
      'idolnum' => 0,
      'tweetnum' => 0,
    );
    $account = (array) $this->user->profile;
    $response = array_merge($profile, $account);
    $this->user->profile->identifier = $response['name'];
    $this->user->profile->displayName = $response['name'];
    $this->user->profile->firstName = $response['first_name'];
    $this->user->profile->lastName = $response['nick'];
    $this->user->profile->description = $response['introduction'];
    $this->user->profile->language = $response['lang'];
    $this->user->profile->address = $response['location'];
    $this->user->profile->profileURL = 'http://t.qq.com/' . $response['name'];
    $this->user->profile->photoURL = $response['head'];
    $this->user->profile->homepage = $response['homepage'];
    $this->user->profile->followerNum = $response['fansnum'];
    $this->user->profile->friendNum = $response['idolnum'];
    $this->user->profile->tweetNum = $response['tweetnum'];
    $this->user->profile->birthDay = $response['birth_day'];
    $this->user->profile->birthMonth = $response['birth_month'];
    $this->user->profile->birthYear = $response['birth_year'];
    $this->user->profile->email = $response['email'];
    switch ($response['sex']) {
      case 1:
        $this->user->profile->gender = t('male');
        break;

      case 0:
        $this->user->profile->gender = t('female');
        break;

    }
    return $this->user->profile;
  }

  /**
   * API function to get data.
   */
  public function api($command, $params = array(), $method = 'POST', $data = NULL) {
    if ($this->getToken('access_token')) {
      // Param setting.
      $params['access_token'] = $this->getToken('access_token');
      $params['oauth_consumer_key'] = self::$clientId;
      $params['openid'] = $this->getToken('openid');
      $params['oauth_version'] = '2.a';
      $params['clientip'] = ip_address();
      $params['scope'] = 'all';
      $params['appfrom'] = 'php-sdk2.0beta';
      $params['seqid'] = time();
      $params['serverip'] = $_SERVER['SERVER_ADDR'];
    }
    $url = self::$apiUrlHttps . trim($command, '/') . '?' . http_build_query($params);
    // Request.
    $r = drupal_http_request($url);
    $r->data = preg_replace('/[^\x20-\xff]*/', "", $r->data);
    $r->data = iconv("utf-8", "utf-8//ignore", $r->data);
    return $r->data;
  }

  /**
   * Get Token.
   */
  public function getToken($key = NULL) {
    if ($key) {
      return $_SESSION['t_' . $this->config['id'] . '_' . $key];
    }
    else {
      return NULL;
    }
  }

  /**
   * Get Authorize URL.
   */
  public static function getAuthorizeURL($redirect_uri, $response_type = 'code', $wap = FALSE) {
    $params = array(
      'client_id' => self::$clientId,
      'redirect_uri' => $redirect_uri,
      'response_type' => $response_type,
      'type' => $type,
    );
    return self::$authorizeURL . '?' . http_build_query($params);
  }

  /**
   * Get Access Token.
   */
  public static function getAccessToken($code, $redirect_uri) {
    $params = array(
      'client_id' => self::$clientId,
      'client_secret' => self::$clientSecret,
      'grant_type' => 'authorization_code',
      'code' => $code,
      'redirect_uri' => $redirect_uri,
    );
    return self::$accessTokenURL . '?' . http_build_query($params);
  }
}
