<?php
/**
 * @file
 * This file provides Sina weibo provider class.
 */

/**
 * Class WeiboProviderSina.
 * WeiboProvider is fixed, Sina is the weibo provider name.
 */
class WeiboProviderSina {
  public $userId;
  protected $config;

  /**
   * Construct class.
   */
  public function __construct($config) {
    $this->config = $config;
    if (ini_get('arg_separator.output') == '&amp;') {
      ini_set('arg_separator.output', '&');
    }
    $this->initialize();
  }

  /**
   * Initializer class.
   */
  public function initialize() {
    if (!$this->config['keys']['key'] || !$this->config['keys']['secret']) {
      throw new Exception('Your application key and secret are required in order to connect to ' . $this->config['id'] . '.', 4);
    }
    $library_path = module_exists('libraries') ? libraries_get_path('sina') : 'sites/all/libraries/sina';
    $file = $library_path . "/saetv2.ex.class.php";
    if (is_file($file)) {
      require_once $file;
    }
    else {
      throw new Exception('Your application PHP SDK saetv2.ex.class.php are required at sites/all/libraries/sina folder.');
    }
    $t_access_token = 't_' . $this->config['id'] . '_access_token';
    if ($_SESSION[$t_access_token]) {
      $this->api = new SaeTClientV2($this->config['keys']['key'], $this->config['keys']['secret'], $_SESSION[$t_access_token]['access_token']);
      $user = $this->api->get_uid();
      $this->userId = $user['uid'];
      if (!$user || !$this->userId) {
        $this->api = new SaeTOAuthV2($this->config['keys']['key'], $this->config['keys']['secret']);
      }
    }
    else {
      $this->api = new SaeTOAuthV2($this->config['keys']['key'], $this->config['keys']['secret']);
    }
  }

  /**
   * Start authenticate.
   */
  public function authenticate() {
    global $base_url;
    $callback = $base_url . '/weibo_connect/redirect/' . $this->config['id'];
    if (isset($this->userId) && $this->userId > 0) {
      $response = $this->api->show_user_by_id($this->userId);
    }
    elseif (isset($_REQUEST['code'])) {
      $keys = array();
      $keys['code'] = $_REQUEST['code'];
      $keys['redirect_uri'] = $callback;
      try {
        $token = $this->api->getAccessToken('code', $keys);
      } catch (OAuthException $e) {
        throw new Exception('Authentification failed! ' . $this->config['id'] . ' returned an invalid Access Token.', 5);
      }
      if ($token) {
        $api = new SaeTClientV2($this->config['keys']['key'], $this->config['keys']['secret'], $token['access_token']);
        $user = $api->get_uid();
        if ($user) {
          $this->userId = $user['uid'];
          $this->api = $api;
          $_SESSION['t_' . $this->config['id'] . '_access_token'] = $token;
          setcookie('weibojs_' . $this->api->client_id, http_build_query($token));
        }
      }
      else {
        exit('<h3>授权失败,请重试</h3>');
      }
    }
    else {
      $url = $this->api->getAuthorizeURL($callback);
      header("Location: $url");
      exit;
    }
  }

  /**
   * Load the user profile from the weibo api client.
   */
  public function getUserProfile() {
    $profile = array(
      'id' => NULL,
      'name' => NULL,
      'screen_name' => NULL,
      'first_name' => NULL,
      'last_name' => NULL,
      'description' => NULL,
      'lang' => NULL,
      'location' => NULL,
      'profile_image_url' => NULL,
      'url' => NULL,
      'age' => NULL,
      'birth_day' => NULL,
      'birth_month' => NULL,
      'birth_year' => NULL,
      'gender' => NULL,
      'email' => NULL,
      'phone' => NULL,
      'address' => NULL,
      'country' => NULL,
      'region' => NULL,
      'city' => NULL,
      'zip' => NULL,
      'followers_count' => 0,
      'friends_count' => 0,
      'statuses_count' => 0,
    );
    $weibo_account = $this->api->show_user_by_id($this->userId);
    $response = array_merge($profile, $weibo_account);
    if ($this->api->oauth->http_code != 200) {
      throw new Exception('User profile request 200 failed! ' . $this->config['id'] . ' returned an error: ' . $response['error'], 6);
    }
    if (!$response) {
      throw new Exception('User profile request response failed! ' . $this->config['id'] . ' api returned an invalid response.', 6);
    }
    $this->user->profile->identifier = $response['id'];
    $this->user->profile->displayName = $response['name'];
    $this->user->profile->firstName = $response['first_name'];
    $this->user->profile->lastName = $response['screen_name'];
    $this->user->profile->description = $response['description'];
    $this->user->profile->language = $response['lang'];
    $this->user->profile->address = $response['location'];
    $this->user->profile->profileURL = 'http://www.weibo.com/u/' . $response['id'];
    $this->user->profile->photoURL = $response['profile_image_url'];
    $this->user->profile->homepage = $response['url'];
    $this->user->profile->followerNum = $response['followers_count'];
    $this->user->profile->friendNum = $response['friends_count'];
    $this->user->profile->tweetNum = $response['statuses_count'];
    switch ($response['gender']) {
      case 'm':
        $this->user->profile->gender = t('male');
        break;

      case 'f':
        $this->user->profile->gender = t('female');
        break;

    }
    return $this->user->profile;
  }
}
