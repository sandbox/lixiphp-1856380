<?php
/**
 * @file
 * This file provides module admin function.
 */

/**
 * Admin form weibo_connect_admin_settings.
 */
function weibo_connect_admin_settings() {
  $providers = _weibo_connect_providers_list();
  $form = array();
  $form['fset_providers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication providers'),
    '#theme' => 'weibo_connect_admin_settings_providers_table',
  );
  foreach ($providers as $provider) {
    $form['fset_providers'][$provider['id']]['id'] = array(
      '#type' => 'hidden',
      '#value' => $provider['id'],
    );
    $form['fset_providers'][$provider['id']]['icon'] = array(
      '#value' => $provider['title'],
      '#provider_id' => $provider['id'],
      '#icon_pack' => 'weibo_16',
    );
    $form['fset_providers'][$provider['id']]['weibo_provider_' . $provider['id'] . '_enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('weibo_provider_' . $provider['id'] . '_enabled', FALSE),
    );
    $form['fset_providers'][$provider['id']]['weibo_provider_' . $provider['id'] . '_weight'] = array(
      '#type' => 'weight',
      '#delta' => 20,
      '#default_value' => variable_get('weibo_provider_' . $provider['id'] . '_weight', $provider['weight']),
      '#attributes' => array('class' => array('weibo-provider-weight')),
    );
    $form['fset_providers'][$provider['id']]['settings'] = array(
      '#value' => l(t('Settings'), 'admin/user/settings/weibo_connect/provider/' . $provider['id'], array('query' => drupal_get_destination())),
    );
  }
  $form['fset_widget'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget settings'),
  );
  $options = array(
    'link' => t('Icons direct link to enabled providers.'),
    'window' => t('Icons popup window of enabled providers'),
  );
  $form['fset_widget']['weibo_connect_widget_type'] = array(
    '#type' => 'radios',
    '#title' => t('Widget type'),
    '#options' => $options,
    '#default_value' => variable_get('weibo_connect_widget_type', 'link'),
  );
  $form['fset_widget']['weibo_connect_login_widget_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Login widget title'),
    '#default_value' => variable_get('weibo_connect_login_widget_title', t('Or log in with...')),
  );
  $options = array(
    '16_16' => 'Default 16px',
    '24_24' => 'Default 24px',
    '32_32' => 'Default 32px',
    '48_48' => 'Default 48px',
  );
  $form['fset_widget']['weibo_connect_login_widget_icon_size'] = array(
    '#type' => 'select',
    '#title' => t('Login widget Icon package'),
    '#options' => $options,
    '#default_value' => variable_get('weibo_connect_login_widget_icon_size', '32_32'),
  );
  $form['fset_widget']['weibo_connect_login_widget_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Login widget weight'),
    '#delta' => 100,
    '#description' => t('Determines the order of the elements on the form - heavier elements get positioned later.'),
    '#default_value' => variable_get('weibo_connect_login_widget_weight', 100),
  );
  $form['fset_other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other settings'),
  );
  $form['fset_other']['weibo_connect_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect after login'),
    '#default_value' => variable_get('weibo_connect_destination', ''),
    '#description' => t('Drupal path to redirect to, like "node/1". Leave empty to return to the same page.'),
  );
  $form['fset_other']['weibo_connect_forms'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Drupal forms'),
    '#options' => array(
      'user_login' => t('User login form'),
      'user_login_block' => t('User login block'),
      'user_register' => t('User registration form'),
      'comment_form' => t('Comment form'),
    ),
    '#default_value' => variable_get('weibo_connect_forms', array('user_login', 'user_login_block')),
    '#description' => t('Add default weibo widget to these forms.'),
  );
  return system_settings_form($form);
}

/**
 * Admin weibo provider form weibo_connect_admin_provider_settings.
 */
function weibo_connect_admin_provider_settings($form_state, $provider_id = NULL) {
  if (!isset($provider_id)) {
    drupal_not_found();
  }
  $providers = _weibo_connect_providers_list();
  $form = array();
  $help = t('Setting the following form, key and/or secret key(required).');
  $form['keys'] = array(
    '#type' => 'fieldset',
    '#description' => $help,
  );
  $form['keys']['weibo_provider_' . $provider_id . '_keys_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#description' => t('The application ID'),
    '#default_value' => variable_get('weibo_provider_' . $provider_id . '_keys_id', ''),
  );
  $form['keys']['weibo_provider_' . $provider_id . '_keys_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Application key'),
    '#description' => t('The Application key'),
    '#default_value' => variable_get('weibo_provider_' . $provider_id . '_keys_key', ''),
  );
  $form['keys']['weibo_provider_' . $provider_id . '_keys_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Application secret'),
    '#description' => t('The application secret key'),
    '#default_value' => variable_get('weibo_provider_' . $provider_id . '_keys_secret', ''),
  );
  if (is_array($providers) && count($providers) > 0 && isset($providers[$provider_id])) {
    $callback = $providers[$provider_id]['callback'];
    if (function_exists($callback)) {
      $callback('settings', $form);
    }
  }
  return system_settings_form($form);
}

/**
 * Theme function for weibo_connect_admin_settings() to render providers table.
 */
function theme_weibo_connect_admin_settings_providers_table($form) {
  $header = array(
    t('Enabled'),
    array('data' => t('Name')),
    t('Weight'),
    array('data' => t('Operations')),
  );
  $rows = array();
  foreach (element_children($form) as $provider_id) {
    if (isset($form[$provider_id]['id'])) {
      $row = array(
        drupal_render($form[$provider_id]['weibo_provider_' . $provider_id . '_enabled']),
        drupal_render($form[$provider_id]['icon']),
        drupal_render($form[$provider_id]['weibo_provider_' . $provider_id . '_weight']),
        drupal_render($form[$provider_id]['settings']),
      );
      $rows[] = array(
        'data' => $row,
      );
    }
  }
  $output = theme('table', $header, $rows, array('id' => 'weibo_connect-providers'));
  $output .= drupal_render($form);
  return $output;
}
