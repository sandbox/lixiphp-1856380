<?php
/**
 * @file
 * Default theme implementation for displaying a login widget in a form.
 * @see template_preprocess_weibo_connect_login_widget()
 */
?>
<?php if($providers): ?>
<div class="form-item" id="weibo-<?php print $widget_id; ?>-wrapper">
  <label for="weibo-<?php print $widget_id; ?>"><?php echo $title; ?></label>
  <ul id="<?php print $widget_id; ?>" class="<?php print $classes; ?> clearfix">
  <?php foreach ($providers as $provider_id => $icon): ?>
    <li><?php print $icon; ?></li>
  <?php endforeach; ?>
  </ul>
  <?php 
  if (!empty($description)):
  ?>
  <div class="description"><?php echo $description;?></div>
  <?php
  endif;
  ?>
</div>
<?php endif; ?>
